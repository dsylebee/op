<?php

/**
 * Created by PhpStorm.
 * User: david
 * Date: 19/01/17
 * Time: 3:14 PM
 */
class ErrorController extends ControllerBase
{
    public function actionIndex($errorCode, $errorMessage) {
        http_response_code(404);
        echo ltrim("{$errorCode} {$errorMessage}", '0 ');
    }
}