<?php

/**
 * Created by PhpStorm.
 * User: david
 * Date: 19/01/17
 * Time: 2:39 PM
 */
class ContactController extends ControllerBase
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionSave() {

        if (empty($_POST["contact"]))
            throw new OpRoutingException("missing form");

        $contact = new Contact();
        $contact->setAttributes($_POST["contact"]);

        if ($contact->hasErrors()) {
            http_response_code(400);
            echo json_encode($contact->errors());
            return;
        }

        $contact->save();
        echo json_encode($contact);
    }

    public function actionList(){
        $contact = new Contact();
        $contacts = $contact->readAll();

        $this->render('list', ['models' => $contacts]);
    }
}