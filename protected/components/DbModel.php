<?php

/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/01/17
 * Time: 12:41 PM
 */
abstract class DbModel extends Model
{
    public abstract function tableName();


    public function save() {
        $primaryAttribute = $this->getPrimaryAttribute();
        if (empty($this->{$primaryAttribute}))
            $this->create();
        else
            $this->update();
    }

    public function getPrimaryAttribute()
    {
        $primaryAttribute = null;
        foreach($this->rules() as $attr => $opts)
        {
            if (!empty($opts["primary"]) && $opts["primary"] === true) {
                $primaryAttribute = $attr;
                break;
            }
        }

        if ($primaryAttribute == null)
            throw new Exception("no primary key defined");

        return $primaryAttribute;
    }

    public function readAll()
    {
        $className = get_called_class();

        $ret = [];
        $sql = "SELECT * FROM {$this->tableName()}";
        $stmt = Op::app()->db()->prepare($sql);
        $stmt->execute();

        $results = $stmt->fetchAll();
        foreach($results as $result) {
            $t = new $className;
            $t->setAttributes($result, false);
            $ret[] = $t;
        }

        return $ret;
    }

    public function readOne($pk)
    {

    }

    public function update()
    {
        $primaryAttribute = $this->getPrimaryAttribute();

        $ret = [];
        $sql = "UPDATE `{$this->tableName()}` SET ";
        $params = [];


        foreach($this->rules() as $attr => $options)
        {
            if ($attr == $primaryAttribute)
                continue;

            $sql .= "`{$attr}` = ?, ";
            $params[] = $this->{$attr};
        }

        $sql = trim(',');
        $sql .= " where $primaryAttribute = ?";
        $params[] = $this->{$primaryAttribute};

        $stmt = Op::app()->db()->prepare($sql);
        $stmt->execute($params);

        return $ret;
    }

    public function create()
    {
        $primaryAttribute = $this->getPrimaryAttribute();

        $ret = [];
        $sql = "INSERT INTO `{$this->tableName()}` (";
        $params = [];


        foreach($this->rules() as $attr => $options)
        {
            if ($attr == $primaryAttribute)
                continue;

            $sql .= "`$attr`,";
            $params[] = $this->{$attr};
        }

        $sql = rtrim($sql, ',') . ") VALUES ( ";
        $bindingMarks = str_repeat("?,", count($this->rules()) -1);
        $sql .= rtrim($bindingMarks, ',');
        $sql .= ')';


        $stmt = Op::app()->db()->prepare($sql);
        $stmt->execute($params);

        // get inserted id
        $this->{$primaryAttribute} = Op::app()->db()->lastInsertId();

        return $ret;
    }

    public function delete()
    {

    }
}