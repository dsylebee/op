<?php

require_once 'Application.php';

/**
 * Class Op
 */
class Op extends Application
{
    private static $instance = null;

    /**
     * @return Op
     */
    public static function app()
    {
        if (Op::$instance == null)
            Op::$instance = new Op();

        return Op::$instance;
    }
}