<?php

/**
 * Created by PhpStorm.
 * User: david
 * Date: 19/01/17
 * Time: 2:40 PM
 */
class ControllerBase
{
    public $action;
    public $layout = 'layouts/main';

    public function runAction($action)
    {
        $this->action = $action;
        $actionMethodName = "action" . ucfirst($this->action);

        if (!method_exists($this, $actionMethodName))
            throw new OpRoutingException("could not find {$actionMethodName} in " . get_called_class());

        $this->{$actionMethodName}();
    }

    public function render($viewName, $params = array())
    {
        // view content
        $controller = Op::app()->controller;
        $viewPath = Op::app()->appDir . "views/$controller/$viewName.php";
        $layoutPath = Op::app()->appDir . "views/$this->layout.php";

        // render the sub content.
        $viewResult = $this->renderView($viewPath, $params, true);

        // render the layout
        $this->renderView($layoutPath, array('content' => $viewResult));
    }

    public function renderPartial($viewName, $params = array())
    {
        // view content
        $controller = Op::app()->controller;
        $viewPath = Op::app()->appDir . "views/$controller/$viewName.php";
        $this->renderView($viewPath, $params);
    }

    protected function renderView($viewPath, $params = array(), $return = false)
    {
        extract($params);

        if ($return) {
            ob_start();
            include $viewPath;
            $result = ob_get_clean();
            return $result;
        }

        include $viewPath;
    }
}