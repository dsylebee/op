<?php

class Application
{
    public $controller = 'site';
    public $action = 'index';
    public $controllerInstance;
    public $appDir;

    protected $dbInstance = null;

    public function __construct() {
        $this->appDir = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;
    }

    public function run()
    {
        $this->registerAutoLoader();
        $this->resolveRoute();

        try {
            $this->runController();
        }catch (OpRoutingException $ex) {
            $this->handleError($ex);
        }
    }

    public function db()
    {
        if ($this->dbInstance == null)
            $this->dbInstance = new Database('mysql:host=localhost;dbname=learn', 'root', '');

        return $this->dbInstance;
    }

    protected function registerAutoLoader()
    {
        spl_autoload_register(array($this, 'autoLoader'));
    }

    protected function autoLoader($className)
    {
        $supportedFolders = ['components', 'controllers', 'models'];
        foreach($supportedFolders as $sf) {
            $assumedFileName = "$this->appDir/$sf/$className.php";
            if (file_exists($assumedFileName)) {
                require_once $assumedFileName;
                break;
            }
        }
    }

    protected function handleError(Exception $ex)
    {
        $errorController = new ErrorController();
        $errorController->actionIndex($ex->getCode(), $ex->getMessage());
    }

    protected function runController()
    {
        // controller
        $controllerClassName = ucfirst($this->controller) . "Controller";

        // class does not exist check.
        if (!class_exists($controllerClassName))
            throw new OpRoutingException("Could not find controller ${controllerClassName}");

        // create class by name,
        $this->controllerInstance = new $controllerClassName;

        // run action
        $this->controllerInstance->runAction($this->action);
    }

    protected function resolveRoute()
    {
        if (empty($_SERVER["REQUEST_URI"]))
            return;

        // sanitize and break url.
        $route = $_SERVER["REQUEST_URI"];
        if (strpos($route, "?") >= 0) {
            $route = explode("?", $route)[0];
        }

        $route = ltrim(rtrim($route, '/'), '/');
        $routeParts = explode('/', $route);

        // resolve which controller and action.
        $this->controller = count($routeParts) > 0 ? $routeParts[0] : $this->controller;
        $this->action = count($routeParts) > 1 ? $routeParts[1] : $this->action;
    }
}