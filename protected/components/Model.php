<?php

/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/01/17
 * Time: 12:24 PM
 */
abstract class Model
{
    protected $errors = [];

    public function setAttributes($attributes, $validate = true)
    {
        foreach($attributes as $attribute => $value)
        {
            if ($attribute === 'errors')
                continue;

            if (!property_exists($this, $attribute))
                continue;

            $this->{$attribute} = $value;
        }

        if ($validate)
            $this->validate();
    }

    public function hasErrors() {
        return !empty($this->errors);
    }

    public function errors() {
        return $this->errors;
    }

    public function validate()
    {
        $this->errors = [];
        $rules = $this->rules();

        foreach($rules as $attr => $attrRules)
        {
            if (isset($attrRules["required"]) && $attrRules["required"] == true && empty($this->{$attr})) {
                $this->errors[$attr][] = "$attr is required";
            }
        }
    }

    public abstract function rules();
}