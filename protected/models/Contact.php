<?php

/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/01/17
 * Time: 12:23 PM
 */
class Contact extends DbModel
{
    public $id;
    public $first_name;
    public $last_name;

    public function rules()
    {
        return [
            'id' => [
                'primary' => true,
                'autoIncrement' => true
            ],
            'first_name' => [
                'required' => true
            ],
            'last_name' => [
                'required' => false
            ]
        ];
    }

    public function tableName() {
        return 'contact';
    }
}