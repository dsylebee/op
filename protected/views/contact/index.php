<form id="contact-form" method="post">

    First Name
    <br>
    <input type="text" name="contact[first_name]" />
    <span style="color: red" data-field-name="contact[first_name]"></span>
    <br>

    LastName
    <br>
    <input type="text" name="contact[last_name]" />
    <span style="color: red" data-field-name="contact[last_name]"></span>
    <br>

    <input type="submit" value="save" />
</form>


<script>
    var $contactForm = $('#contact-form');
    $contactForm.submit(function(e) {
        e.preventDefault();

        var formData = $(this).serialize();

        /* old fashions
        $.ajax({
            type: 'POST',
            url: '/contact/save',
            dataType: 'json',
            data: formData,
            success: function(result) {
                console.log(result);
            },
            error: function(xhr) {
                console.log(xhr);
            },
            complete: function() {

            }
        });*/

        var promise = $.ajax({
            type: 'POST',
            url: '/contact/save',
            dataType: 'json',
            data: formData
        });

        promise
            .then(function(result) {

            })
            .fail(function(xhr) {
                try {
                    var errors = JSON.parse(xhr.responseText);
                    $.each(errors, function(attr, errors) {

                        var mergeStringArrayReduceCallBack = function(prev, next) {
                            return prev + ' ' + next;
                        };

                        var errorSummaryOfField = errors.reduce(mergeStringArrayReduceCallBack, '');

                       $('[data-field-name="contact[' + attr + ']"]').html(errorSummaryOfField);
                    });
                } catch(e) {

                }
            })
            .always(function() {

            });
    });
</script>